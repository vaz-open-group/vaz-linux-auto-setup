#!/bin/bash
URL_WINE_KEY="https://dl.winehq.org/wine-builds/winehq.key"
URL_PPA_WINE="https://dl.winehq.org/wine-builds/ubuntu/"
URL_GOOGLE_CHROME="https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
URL_SIMPLE_NOTE="https://github.com/Automattic/simplenote-electron/releases/download/v1.8.0/Simplenote-linux-1.8.0-amd64.deb"
URL_4K_VIDEO_DOWNLOADER="https://dl.4kdownload.com/app/4kvideodownloader_4.9.2-1_amd64.deb"
URL_INSYNC="https://d2t3ff60b2tol4.cloudfront.net/builds/insync_3.0.20.40428-bionic_amd64.deb"
URL_ANACONDA_REPO_BASE="https://repo.anaconda.com/archive/"
URL_ANACONDA_CURRENT="https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh"
DOWNLOAD_DIR="/home/$USER/Downloads/__apps__"

echo "**************************************************"
echo "**************************************************"
echo "            Vaz Linux Automatic Setup             "
echo "**************************************************"
echo "**************************************************"
echo "--------------------------------------------------"
echo "Creating dir for downloads"
echo "--------------------------------------------------"
# mkdir $DOWNLOAD_DIR
# cd $DOWNLOAD_DIR

echo "--------------------------------------------------"
echo "Updating Repositories"
echo "--------------------------------------------------"
# sudo apt-get update

echo "--------------------------------------------------"
echo "            Installing Essential Apps             "
echo "--------------------------------------------------"


echo "--------------------------------------------------"
echo ">>>>> Installing ZSH <<<<<"
echo "--------------------------------------------------"

echo ">>>>> Downloading Google Chrome <<<<<"
# wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
echo ">>>>> Installing Google Chrome <<<<<"


echo ">>>>> Download External Apps <<<<<"
echo ">>>>> Installing Development Apps (tools) <<<<<"
echo "Downloading VSCode"
# wget -c https://go.microsoft.com/fwlink/?LinkID=760868

echo "Downloading Anaconda"
echo "Installing Anaconda"
echo "Configuring Anaconda"
echo "Download PyQt"
echo "Install PyQt"
echo "Installing MySQL Workbench (repo)"
echo "Installing Postman (repo)"
echo "Download STMicroeletronics STM Cube"
echo "Install STMicroeletronics STM Cube"
echo "Install CuteCom (repo)"
echo "Installing Android Studio (repo)"
echo "Installing FreeCAD (repo)"
echo "Download GitAhead"
echo "Install GitAhead"
echo "Installing Sublime (repo)"
echo "Installing NotepadQQ (repo)"
echo ">>>>> Installing Python Packs <<<<<"
echo "Installing Telepot"
echo "Installing Matplotlib"
echo "Installing Flask"
echo "Installing Scipy"
echo "Installing mysql-connector"
echo "Installing Numpy"
echo "Installing Pandas"
echo "Installing boto3"
echo "Installing Werkzeug"
echo ">>>>> Installing Util Apps <<<<<"
echo "Install RedShift"
echo "Install HTop (repo)"
echo "Install TeamViewer (repo)"
echo "Install Virtual Box (repo)"
echo "Install Wireshark (repo)"
echo "Install Terminator (repo)"
echo "Install Audacity (repo)"
echo "Install Cheese (repo)"
echo ">>>>> Configure Environment <<<<<"
echo ">>>>> Deploying Backuped Items <<<<<"
echo ">>>>> Deploying Pictures <<<<<"
echo ">>>>> Deploying Google Chrome Favorites <<<<<"
echo ">>>>> Deploying Google Chrome Section <<<<<"
echo ">>>>> Configure Linux Design <<<<<"
echo ">>>>> Add PPA Repositories <<<<<"
